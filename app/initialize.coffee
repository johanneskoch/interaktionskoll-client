application = require 'application'

$ ->
  $(document).ajaxComplete (event, jqxhr) ->
    try
      alert = JSON.parse(jqxhr.responseText).alert
    if alert?
      application.alerts.trigger alert.level, alert.message
    else if jqxhr.status < 200 or jqxhr.status >= 300 and jqxhr.status != 304
      application.alerts.trigger 'danger', 'A server error occurred.'

  $(document).ajaxError (event, jqxhr) ->
    if jqxhr.status == 401
      $(document).trigger 'unauthorized'

  Backbone.Model.prototype.parse = Backbone.Collection.prototype.parse = (resp, xhr) ->
    resp.data or resp

  application.initialize()
  Backbone.history.start()
