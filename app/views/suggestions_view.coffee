DrugList = require 'models/drug_collection'
SuggestionsItemView = require 'views/suggestions_item_view'

module.exports = class SuggestionsView extends Backbone.View
  initialize: (options) =>
    {@input, @handler} = options
    @collection = new DrugList

    $('html').bind 'click', => @$el.hide()  # binding to @input.blur was buggy
    @input
      .bind('click', (event) -> event.stopPropagation())
      .bind('focus', => @$el.show())
      .bind('keyup', @keyUp)
      .bind 'keyup', _.debounce @debouncedKeyUp, 300
    @collection.bind 'reset', @addAll

  keyUp: (event) =>
    switch event.which
      when 13
        if @collection.highlighted?
          @selectSuggestion @collection.highlighted
      when 38 then @collection.highlightPrevious()
      when 40 then @collection.highlightNext()

  debouncedKeyUp: =>
    @showSuggestions @input.val()

  showSuggestions: (value) =>
    if value != @value
      if value.length < 3
        @collection.reset()
      else
        @collection.fetch
          url: '/search'
          data: q: value
          reset: true
      @value = value

  selectSuggestion: (model) =>
    @handler model
    @input.val ''
    @collection.reset()
    @value = null

  addOne: (model) =>
    view = new SuggestionsItemView
      model: model
      handler: @selectSuggestion
    @$el.find('ul').append view.render().el

  addAll: =>
    @$el
      .toggleClass('open', !!@collection.length)
      .find('ul').empty()
    @collection.each @addOne
