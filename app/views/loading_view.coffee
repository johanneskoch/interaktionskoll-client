module.exports = class LoadingView extends Backbone.View
  initialize: =>
    $(document)
      .ajaxStart(@start)
      .ajaxStop @stop

  start: =>
    @$el
      .width(0)
      .show()
      .animate width: "#{ 50 + Math.random() * 30 }%", 250

  stop: =>
    @$el.animate width: '100%', 250, -> $(@).fadeOut()
