module.exports = class AlertsItemView extends Backbone.View
  template: require './templates/alerts_item'

  events:
    'click .close': 'remove'

  initialize: (options) =>
    if !options.persist
      setTimeout (=> @$el.fadeOut => @remove()), 5000

  render: =>
    @$el.html @template @options
    @
