application = require 'application'

module.exports = class SearchItemView extends Backbone.View
  className: 'btn-group btn-group-lg'
  template: require './templates/search_item'

  events:
    'click .search-item-interactions': 'showInteractions'
    'click .search-item-delete': 'delete'

  initialize: =>
    @model.bind 'destroy', => @remove()  # not sure why the closure is needed

  showInteractions: =>
    application.collections.currentInteractions.fetch
      url: @model.get 'interactions_uri'

  delete: =>
    @model.trigger 'destroy', @model

  render: =>
    @$el.html @template @model.toJSON()
    @
