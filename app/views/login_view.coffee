application = require 'application'

module.exports = class LoginView extends Backbone.View
  template: require './templates/login'

  events:
    'submit form': 'submitForm'

  initialize: =>
    @model.bind 'change:logged_in', @render

    @render()

  submitForm: (event) =>
    event.preventDefault()
    @login()

  login: =>
    $.ajax
      url: '/user/login'
      type: 'POST'
      data: @$el.find('form').serialize()
      success: =>
        @model.fetch()

  render: =>
    @$el.html @template @model.toJSON()
    @
