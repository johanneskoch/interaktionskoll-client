application = require 'application'
SearchItemView = require 'views/search_item_view'
SuggestionsView = require 'views/suggestions_view'

module.exports = class SearchView extends Backbone.View
  template: require './templates/search'

  events:
    'click .search-button': 'showInteractions'

  initialize: =>
    @collection.bind 'add', @addOne
    @collection.bind 'reset', @addAll

    @render()

    new SuggestionsView
      el: @$el.find '#suggestions-view'
      input: @$el.find '#search-view-form input'
      handler: @selectSuggestion

  selectSuggestion: (model) =>
    @collection.add model

  showInteractions: =>
    application.collections.currentInteractions.fetch
      data: $.param @collection.map (drug) -> name: drug.get('type'), value: drug.get 'name'

  addOne: (model) =>
    view = new SearchItemView model: model
    @$el.find('#search-view-list').append view.render().el

  addAll: =>
    @$el.find('#search-view-list').empty()
    @collection.each @addOne

  render: =>
    @$el.html @template()
    @
