module.exports = class SuggestionsItemView extends Backbone.View
  tagName: 'li'
  template: require './templates/suggestions_item'

  events:
    'click a': 'select'

  initialize: (options) =>
    @handler = options.handler
    @model.bind 'change:highlighted', @render

  select: =>
  	@handler @model

  render: =>
    @$el.html @template @model.toJSON()
    @
