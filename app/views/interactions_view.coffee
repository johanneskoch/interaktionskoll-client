application = require 'application'
InteractionsItemView = require 'views/interactions_item_view'

module.exports = class InteractionsView extends Backbone.View
  template: require './templates/interactions'

  events:
    'click .back-button': 'gotoSearch'

  initialize: =>
    @collection.bind 'reset', @addAll

    @render()

  gotoSearch: ->
    application.router.navigate 'search', trigger: true

  addOne: (model) =>
    view = new InteractionsItemView model: model
    @$el.find('#interactions-view-list').append view.render().el

  addAll: =>
    @$el.find('#interactions-view-list').empty()
    @collection.each @addOne

  render: =>
    @$el.html @template()
    @
