AlertsItemView = require 'views/alerts_item_view'

module.exports = class AlertsView extends Backbone.View
  initialize: (options) =>
    options.dispatcher.bind 'all', @alert

  alert: (level, message, persist=false) =>
    view = new AlertsItemView
      level: level
      message: message
      persist: persist
    @$el.prepend view.render().el
