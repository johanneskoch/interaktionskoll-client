module.exports = class InteractionsItemView extends Backbone.View
  className: 'interaction'
  template: require './templates/interactions_item'

  events:
    'click': 'toggle'
    'click a': 'clickReferenceLink'

  initialize: =>
    @model.bind 'change', @render

  toggle: =>
    if @$el.hasClass('open') then @close() else @open()

  open: =>
    @model.fetchIfNeeded()
    # $('html, body').scrollTop @$el.offset().top - 10  # disorienting
    @$el.addClass 'open'

  close: =>
    @$el.removeClass 'open'

  clickReferenceLink: (event) =>
    event.stopPropagation()

  render: =>
    @$el.html @template _.extend @model.toJSON(),
      hasDetails: @model.hasDetails()
    @
