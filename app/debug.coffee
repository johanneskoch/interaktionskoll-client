# open -a Google\ Chrome --args --disable-web-security
console.log 'DEBUG MODE'

rootUrl = 'https://interaktionskoll.se'
jQueryAjax = $.ajax

$.ajax = (url, options) ->
  if typeof url == 'object'
    url.url = rootUrl + url.url
  else
    url = rootUrl + url

  jQueryAjax url, options
