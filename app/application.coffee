Application =
  initialize: ->
    User = require 'models/user_model'
    DrugList = require 'models/drug_collection'
    InteractionList = require 'models/interaction_collection'
    LoadingView = require 'views/loading_view'
    AlertsView = require 'views/alerts_view'
    LoginView = require 'views/login_view'
    SearchView = require 'views/search_view'
    InteractionsView = require 'views/interactions_view'
    Router = require 'lib/router'

    @alerts = _.clone Backbone.Events

    @user = new User

    @collections =
      currentDrugs: new DrugList
      currentInteractions: new InteractionList

    @views =
      loading: new LoadingView
        el: '#loading-view'
      alerts: new AlertsView
        el: '#alerts-view'
        dispatcher: @alerts
      login: new LoginView
        el: '#login-view'
        model: @user
      search: new SearchView
        el: '#search-view'
        collection: @collections.currentDrugs
      interactions: new InteractionsView
        el: '#interactions-view'
        collection: @collections.currentInteractions

    @router = new Router

    @user.bind 'change:logged_in', (model, value) =>
      if value then @router.navigate 'search', trigger: true
    @user.fetch()

    $(document).bind 'unauthorized', =>
      @user.set logged_in: false
      @router.navigate '', trigger: true

    Object.freeze? @

module.exports = Application
