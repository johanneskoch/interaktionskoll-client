Drug = require 'models/drug_model'

module.exports = class DrugList extends Backbone.Collection
  model: Drug

  initialize: =>
    @bind 'reset', @resetHighlight

  highlightPrevious: =>
    index = @indexOf @highlighted
    if index < 1
      @highlight @length - 1
    else
      @highlight index - 1

  highlightNext: =>
    index = @indexOf @highlighted
    if index == -1 or index == @length - 1
      @highlight 0
    else
      @highlight index + 1

  highlight: (index) =>
    @highlighted?.set highlighted: false
    @highlighted = @at index
    @highlighted?.set highlighted: true

  resetHighlight: =>
    @highlighted = null
