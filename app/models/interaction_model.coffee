module.exports = class Interaction extends Backbone.Model
  displayClasses:
    A: 'success'
    B: 'info'
    C: 'warning'
    D: 'danger'

  initialize: =>
    @set
      displayClass: @displayClasses[@get('classification').charAt 0]
      displayTitle: _.pluck(@get('substances'), 'name').join ', '

  hasDetails: =>
    @has 'mechanism'

  fetchIfNeeded: =>
    if !@fetchingDetails
      @fetchingDetails = true
      @fetch()
