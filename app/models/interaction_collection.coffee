application = require 'application'
Interaction = require 'models/interaction_model'

module.exports = class InteractionList extends Backbone.Collection
  model: Interaction
  url: '/interactions'

  fetch: (options={}) ->
    defaultOptions =
      reset: true
      success: (collection) ->
        if collection.length
          application.router.navigate 'interactions', trigger: true

    super _.extend defaultOptions, options
