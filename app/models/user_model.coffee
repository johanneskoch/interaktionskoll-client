module.exports = class User extends Backbone.Model
  url: -> '/user'

  fetch: (options={}) ->
    super _.extend {cache: false}, options
