application = require 'application'

activateView = (view) ->
  view.$el.addClass('active').siblings().removeClass 'active'

module.exports = class Router extends Backbone.Router
  routes:
    '': 'login'
    'search': 'search'
    'interactions': 'interactions'

  login: ->
    activateView application.views.login

  search: ->
    activateView application.views.search

  interactions: ->
    activateView application.views.interactions
