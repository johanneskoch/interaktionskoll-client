Interaktionskoll Client
=======================

A simple client/web app for the [Interaktionskoll] project, built using:

* the [Backbone.js] JavaScript framework
* the [Bootstrap] CSS framework
* the [Brunch] build tool
* the [CoffeeScript] language

Familiarity with these is obviously helpful if you want to play around with the client. Alternatively, this might be a useful project for getting to know those components you haven't tried out yet, as it's fairly small.

Setup
-----

Before you are able to do anything with this, you more or less need to get the [Interaktionskoll] project running. If you haven't already, head over there and do that first.

Note that the default configuration assumes you have the two repositories organized like this:

    /interaktionskoll/ (virtualenv)
      /interaktionskoll/
      /interaktionskoll-client/

If you use a different directory structure, you have to modify the path `../interaktionskoll/server/static` in the file `config.coffee`, otherwise the compiled static assets will end up God-knows-where.

Installation
------------

If you've worked with *Node.js* before, this should only take a minute. Everyone else, head over to [http://nodejs.org/] and install it. The process varies for each OS, so details won't be provided here. At the end of it, you should have Node.js, as well as the *Node Package Manager* (npm) that comes bundled with it, installed. After that, run these commands:

```sh
git clone https://johanneskoch@bitbucket.org/johanneskoch/interaktionskoll-client.git
cd interaktionskoll-client
sudo npm install -g brunch
npm install
```

To build the project, simply run `brunch watch` inside the repository, and Brunch will automatically recompile the assets whenever anything changes.

[Interaktionskoll]:https://bitbucket.org/johanneskoch/interaktionskoll
[Backbone.js]:http://backbonejs.org/
[Bootstrap]:http://getbootstrap.com/
[Brunch]:http://brunch.io/
[CoffeeScript]:http://coffeescript.org/
[http://nodejs.org/]:http://nodejs.org/
